#ifndef _GAMEMATH_H
#define _GAMEMATH_H

static double distanceSquared(int x1, int y1, int x2, int y2) {
	int deltaX = x2 - x1;
	int deltaY = y2 - y1;
	return deltaX*deltaX + deltaY*deltaY;
}

#endif /* _GAMEMATH_H */