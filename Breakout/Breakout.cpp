#include "Breakout.h"

Breakout::Breakout() {
	mWindow = NULL;
	mRenderer = NULL;
	mPoints = 0;
	mBalls = 2;
	mGameOver = false;
	mLevelUp = true;
	mWin = false;
	mGameOverAlpha = 0;
	mMouseX = 0;
	mMouseY = 0;
	mBlocksLeft = 0;
	mLevel = 0;
	mRows = mLevel + 4;
	mBlocks.reserve(500);
}

Breakout::~Breakout() {
	SDL_DestroyWindow(mWindow);
	SDL_DestroyRenderer(mRenderer);

	mPlayerTexture.free();
	mBallTexture.free();
	mRedBlockTexture.free();
	mOrangeBlockTexture.free();
	mYellowBlockTexture.free();
	mGreenBlockTexture.free();
	mSkyBlueBlockTexture.free();
	mBlueBlockTexture.free();
	mPurpleBlockTexture.free();
	mBallTexture.free();
	mBallsTexture.free();
	mPointsTexture.free();
	mGameOverTexture.free();
	mPlayAgainTexture.free();
	mPlayAgainPushedTexture.free();
	mWinTexture.free();

	mMixer.free();

	SDL_Quit();
	TTF_Quit();
	IMG_Quit();
}

bool Breakout::init() {
	bool success = true;

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		success = false;
	} else {
		mWindow = SDL_CreateWindow("Breakout", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (mWindow == NULL) {
			SDL_Log("Unable to create Window: %s", SDL_GetError());
			success = false;
		} else {
			mRenderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if (mRenderer == NULL) {
				SDL_Log("Unable to create Renderer", SDL_GetError());
				success = false;
			} else {
				if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
					SDL_Log("Unable to initialize mixer: %s", SDL_GetError());
					success = false;
				} else {
					if (TTF_Init() == -1) {
						SDL_Log("Unable to initialize TTF: %s", TTF_GetError());
						success = false;
					} else {
						if (!loadMedia()) {
							success = false;
						}
					}
				}
			}
		}
	}

	return success;
}

bool Breakout::loadMedia() {
	bool success = true;

	mFont = TTF_OpenFont("OpenSans-Regular.ttf", 18);
	if (mFont == NULL) {
		success = false;
	} else {
		if (!mPlayerTexture.loadMediaFromFile(mRenderer, "Player.png")) {
			success = false;
		} else {
			mPlayer.setTexture(&mPlayerTexture);

			if (!mBallTexture.loadMediaFromFile(mRenderer, "ball.png")) {
				success = false;
			} else {
				mBall.setTexture(&mBallTexture);

				if (!mRedBlockTexture.loadMediaFromFile(mRenderer, "redblock.png")) {
					success = false;
				} else {
					if (!mOrangeBlockTexture.loadMediaFromFile(mRenderer, "orangeblock.png")) {
						success = false;
					} else {
						if (!mYellowBlockTexture.loadMediaFromFile(mRenderer, "yellowblock.png")) {
							success = false;
						} else {
							if (!mGreenBlockTexture.loadMediaFromFile(mRenderer, "greenblock.png")) {
								success = false;
							} else {
								if (!mSkyBlueBlockTexture.loadMediaFromFile(mRenderer, "skyblueblock.png")) {
									success = false;
								} else {
									if (!mBlueBlockTexture.loadMediaFromFile(mRenderer, "blueblock.png")) {
										success = false;
									} else {
										if (!mPurpleBlockTexture.loadMediaFromFile(mRenderer, "purpleblock.png")) {
											success = false;
										} else {
											if (!mPointsTexture.loadFontFromFile(mRenderer, mFont, ("Points: " + std::to_string(mPoints)).c_str())) {
												success = false;
											} else {
												if (!mBallsTexture.loadFontFromFile(mRenderer, mFont, ("x" + std::to_string(mBalls)).c_str())) {
													success = false;
												} else {
													if (!mGameOverTexture.loadMediaFromFile(mRenderer, "youdied.png")) {
														success = false;
													} else {
														if (!mPlayAgainTexture.loadMediaFromFile(mRenderer, "playagain.png")) {
															success = false;
														} else {
															if (!mPlayAgainPushedTexture.loadMediaFromFile(mRenderer, "playagainpushed.png")) {
																success = false;
															} else {
																if (!mWinTexture.loadMediaFromFile(mRenderer, "youwin.png")) {
																	success = false;
																} else {
																	if (!loadLevelTextures()) {
																		success = false;
																	} else {
																		addObservers();

																		if (!mMixer.setSounds()) {
																			success = false;
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return success;
}

void Breakout::spawnEntities() {
	int blockWidth = mRedBlockTexture.getWidth();
	int blockLimit = SCREEN_WIDTH / blockWidth;
	int x = SCREEN_WIDTH - blockWidth * blockLimit;
	Texture* blockTexture = NULL;
	mBlocksLeft = blockLimit * mRows;

	for (int i = 0; i < mBlocksLeft; i++) {
		switch ((i / blockLimit) % 7) {
			case 0:
				blockTexture = &mRedBlockTexture;
				break;
			case 1:
				blockTexture = &mOrangeBlockTexture;
				break;
			case 2:
				blockTexture = &mYellowBlockTexture;
				break;
			case 3:
				blockTexture = &mGreenBlockTexture;
				break;
			case 4:
				blockTexture = &mSkyBlueBlockTexture;
				break;
			case 5:
				blockTexture = &mBlueBlockTexture;
				break;
			case 6:
				blockTexture = &mPurpleBlockTexture;
				break;
		}

		Block block = Block(i * blockWidth - (SCREEN_WIDTH - x) * (i / blockLimit) + (x / 2), (int)(100 + mRedBlockTexture.getHeight() * (i / blockLimit)), blockTexture);
		block.addObserver(this);
		mBlocks.push_back(block);
	}
}

bool Breakout::handleEvent() {
	SDL_Event e;

	//Handling events
	while (SDL_PollEvent(&e) != 0) {
		//User requests quit
		if (e.type == SDL_QUIT) {
			return false;
		} else if (e.type == SDL_MOUSEMOTION) {
			SDL_GetMouseState(&mMouseX, &mMouseY);
		} else if (e.type == SDL_MOUSEBUTTONDOWN) {
			if (e.button.button = SDL_BUTTON_LEFT) {
				if (mGameOver || mWin) {
					if (mMouseX >= SCREEN_WIDTH / 2 - mPlayAgainTexture.getWidth() / 2 && mMouseX <= SCREEN_WIDTH / 2 + mPlayAgainTexture.getWidth() / 2
						&& mMouseY >= SCREEN_HEIGHT * .75  && mMouseY <= SCREEN_HEIGHT * .75 + mPlayAgainTexture.getHeight()) {
						restartGame();
					}
				}
			}
		}

		if (!mGameOver && !mLevelUp && !mWin) {
			mPlayer.handleEvent(e);
			mBall.handleEvent(e);
		}
	}

	return true;
}

void Breakout::update() {
	if (!mGameOver) {
		mPlayer.move();
		mBall.move(mPlayer.getPosX() + mPlayer.getWidth() / 2, mPlayer.getPosY() - mPlayer.getHeight(), mPlayer.getCollisionBox(), mBlocks);

		if (mLevelUp) {
			if (mTimer.getTicks() >= 2000) {
				mLevelUp = false;
				spawnEntities();
			}
		}
	}
}

void Breakout::render(double time) {
	SDL_RenderClear(mRenderer);
	SDL_SetRenderDrawColor(mRenderer, 255, 255, 255, 255);

	mBall.render(mRenderer, time, mPlayer.getVelocity());
	mPlayer.render(mRenderer, time);
	mPointsTexture.render(mRenderer, 5, SCREEN_HEIGHT - mPointsTexture.getHeight());
	mBallsTexture.render(mRenderer, SCREEN_WIDTH / 2 - mBallsTexture.getWidth(), SCREEN_HEIGHT - mBallsTexture.getHeight());
	mBallTexture.render(mRenderer, SCREEN_WIDTH / 2 - mBallTexture.getWidth() - mBallsTexture.getWidth(), SCREEN_HEIGHT - mBallTexture.getHeight() - 3);
	mLevelTextTexture.render(mRenderer, SCREEN_WIDTH - mLevelTextTexture.getWidth() - 5, SCREEN_HEIGHT - mLevelTextTexture.getHeight());

	std::vector<Block>::iterator it;
	for (it = mBlocks.begin(); it != mBlocks.end(); it++) {
		if ((*it).isActive()) {
			(*it).render(mRenderer);
		}
	}

	if (mGameOver) {
		if (mGameOverAlpha < 200) {
			mGameOverAlpha += .5;
			mGameOverTexture.setAlpha((int)mGameOverAlpha);
			mGameOverTexture.render(mRenderer, 0, SCREEN_HEIGHT / 2 - mGameOverTexture.getHeight() / 2);
		}

		if (mGameOverAlpha == 200) {
			mGameOverTexture.render(mRenderer, 0, SCREEN_HEIGHT / 2 - mGameOverTexture.getHeight() / 2);
		}

		mPointsTexture.render(mRenderer, SCREEN_WIDTH / 2 - mPointsTexture.getWidth() / 2, SCREEN_HEIGHT / 2 + mGameOverTexture.getHeight() / 2 + mPointsTexture.getHeight() / 2);

		if (mMouseX >= SCREEN_WIDTH / 2 - mPlayAgainTexture.getWidth() / 2 && mMouseX <= SCREEN_WIDTH / 2 + mPlayAgainTexture.getWidth() / 2
									&& mMouseY >= SCREEN_HEIGHT * .75  && mMouseY <= SCREEN_HEIGHT * .75 + mPlayAgainTexture.getHeight()) {
			mPlayAgainPushedTexture.render(mRenderer, SCREEN_WIDTH / 2 - mPlayAgainTexture.getWidth() / 2, SCREEN_HEIGHT * .75);
		} else {
			mPlayAgainTexture.render(mRenderer, SCREEN_WIDTH / 2 - mPlayAgainTexture.getWidth() / 2, SCREEN_HEIGHT * .75);
		}
	}

	if (mLevelUp && !mWin) {
		SDL_RenderClear(mRenderer);
		mLevelTexture.render(mRenderer, SCREEN_WIDTH / 2 - mLevelTexture.getWidth() / 2, SCREEN_HEIGHT / 2 - mLevelTexture.getHeight() / 2);
		/*SDL_Rect clip = { 75 * (mLevel - 1), 0, 75, 104 };
		mNumberSheetTexture.render(mRenderer, SCREEN_WIDTH / 2 - (75 / 2), SCREEN_HEIGHT / 2 + mLevelTexture.getWidth() / 2, &clip);*/
		switch (mLevel){
			case 1:
				mOneTexture.render(mRenderer, SCREEN_WIDTH / 2 + mLevelTexture.getWidth() / 2 + 5, SCREEN_HEIGHT / 2 - mLevelTexture.getHeight() / 2);
				break;
			case 2:
				mTwoTexture.render(mRenderer, SCREEN_WIDTH / 2 + mLevelTexture.getWidth() / 2 + 5, SCREEN_HEIGHT / 2 - mLevelTexture.getHeight() / 2);
				break;
			case 3:
				mThreeTexture.render(mRenderer, SCREEN_WIDTH / 2 + mLevelTexture.getWidth() / 2 + 5, SCREEN_HEIGHT / 2 - mLevelTexture.getHeight() / 2);
				break;
			case 4:
				mFourTexture.render(mRenderer, SCREEN_WIDTH / 2 + mLevelTexture.getWidth() / 2 + 5, SCREEN_HEIGHT / 2 - mLevelTexture.getHeight() / 2);
				break;
			case 5:
				mFiveTexture.render(mRenderer, SCREEN_WIDTH / 2 + mLevelTexture.getWidth() / 2 + 5, SCREEN_HEIGHT / 2 - mLevelTexture.getHeight() / 2);
				break;
			case 6:
				mSixTexture.render(mRenderer, SCREEN_WIDTH / 2 + mLevelTexture.getWidth() / 2 + 5, SCREEN_HEIGHT / 2 - mLevelTexture.getHeight() / 2);
				break;
			case 7:
				mSevenTexture.render(mRenderer, SCREEN_WIDTH / 2 + mLevelTexture.getWidth() / 2 + 5, SCREEN_HEIGHT / 2 - mLevelTexture.getHeight() / 2);
				break;
			case 8:
				mEightTexture.render(mRenderer, SCREEN_WIDTH / 2 + mLevelTexture.getWidth() / 2 + 5, SCREEN_HEIGHT / 2 - mLevelTexture.getHeight() / 2);
				break;
			case 9:
				mNineTexture.render(mRenderer, SCREEN_WIDTH / 2 + mLevelTexture.getWidth() / 2 + 5, SCREEN_HEIGHT / 2 - mLevelTexture.getHeight() / 2);
				break;
		}
	}

	if (mWin) {
		SDL_RenderClear(mRenderer);

		mWinTexture.render(mRenderer, SCREEN_WIDTH / 2 - mWinTexture.getWidth() / 2, SCREEN_HEIGHT / 2 - mWinTexture.getHeight() / 2);
		mPointsTexture.render(mRenderer, SCREEN_WIDTH / 2 - mPointsTexture.getWidth() / 2, SCREEN_HEIGHT / 2 + mWinTexture.getHeight() / 2 + mPointsTexture.getHeight() / 2);

		if (mMouseX >= SCREEN_WIDTH / 2 - mPlayAgainTexture.getWidth() / 2 && mMouseX <= SCREEN_WIDTH / 2 + mPlayAgainTexture.getWidth() / 2
			&& mMouseY >= SCREEN_HEIGHT * .75  && mMouseY <= SCREEN_HEIGHT * .75 + mPlayAgainTexture.getHeight()) {
			mPlayAgainPushedTexture.render(mRenderer, SCREEN_WIDTH / 2 - mPlayAgainTexture.getWidth() / 2, SCREEN_HEIGHT * .75);
		} else {
			mPlayAgainTexture.render(mRenderer, SCREEN_WIDTH / 2 - mPlayAgainTexture.getWidth() / 2, SCREEN_HEIGHT * .75);
		}
	}

	SDL_RenderPresent(mRenderer);
}

void Breakout::toggleMusic() {
	mMixer.toggleMusic();
}

void Breakout::addObservers() {
	mBall.addObserver(&mMixer);
	mBall.addObserver(this);

	addObserver(&mMixer);
}

void Breakout::gameOver() {
	mGameOver = true;
	notify(GAME_OVER);
}

void Breakout::win() {
	mWin = true;
	notify(WIN);
}

void Breakout::restartGame() {
	mGameOver = false;
	mWin = false;
	notify(RESTART_GAME);
	mBalls = 2;
	mPoints = 0;
	mLevel = 0;
	mRows = mLevel + 4;
	setText();
	mBlocks.clear();
	levelUp();
}

void Breakout::levelUp() {
	mLevelUp = true;
	++mBalls;
	++mLevel;
	++mRows;

	if (mLevel >= 10) {
		win();
	}

	mBall.reset();
	mPlayer.reset();
	mTimer.start();
	setText();
}

void Breakout::setText() {
	mPointsTexture.loadFontFromFile(mRenderer, mFont, ("Points: " + std::to_string(mPoints)).c_str());
	mBallsTexture.loadFontFromFile(mRenderer, mFont, ("x" + std::to_string(mBalls)).c_str());
	mLevelTextTexture.loadFontFromFile(mRenderer, mFont, ("Level: " + std::to_string(mLevel)).c_str());
}

bool Breakout::loadLevelTextures() {
	bool success = true;

	if (!mLevelTexture.loadMediaFromFile(mRenderer, "level.png")) {
		success = false;
	} else {
		if (!mNumberSheetTexture.loadMediaFromFile(mRenderer, "numbersheet.png")) {
			success = false;
		}
		if (!mOneTexture.loadMediaFromFile(mRenderer, "1.png")) {
			success = false;
		} else {
			if (!mTwoTexture.loadMediaFromFile(mRenderer, "2.png")) {
				success = false;
			} else {
				if (!mThreeTexture.loadMediaFromFile(mRenderer, "3.png")) {
					success = false;
				} else {
					if (!mFourTexture.loadMediaFromFile(mRenderer, "4.png")) {
						success = false;
					} else {
						if (!mFiveTexture.loadMediaFromFile(mRenderer, "5.png")) {
							success = false;
						} else {
							if (!mSixTexture.loadMediaFromFile(mRenderer, "6.png")) {
								success = false;
							} else {
								if (!mSevenTexture.loadMediaFromFile(mRenderer, "7.png")) {
									success = false;
								} else {
									if (!mEightTexture.loadMediaFromFile(mRenderer, "8.png")) {
										success = false;
									} else {
										if (!mNineTexture.loadMediaFromFile(mRenderer, "9.png")) {
											success = false;
										} else {
											if (!mZeroTexture.loadMediaFromFile(mRenderer, "0.png")) {
												success = false;
											} else {
												if (!mLevelTextTexture.loadFontFromFile(mRenderer, mFont, ("Level: " + std::to_string(mLevel)).c_str())) {
													success = false;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return success;
}

void Breakout::onNotify(GameEvent event) {
	switch (event) {
		case BLOCK_HIT:
			mPoints += 50;
			--mBlocksLeft;

			if (mBlocksLeft <= 0) {
				levelUp();
			}

			setText();
			break;
		case BALL_OUT_OF_BOUNDS:
			--mBalls;
			if (mBalls <= 0) {
				gameOver();
			}
			setText();
			break;
	}
}