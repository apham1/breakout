#ifndef _MIXER_H
#define _MIXER_H

#include <SDL.h>
#include <SDL_mixer.h>
#include "Observer.h"

class Mixer : public Observer{
public:
	Mixer();

	bool setSounds();
	void setVolume(int volume);
	void toggleMusic();
	void pauseMusic();
	void unPauseMusic();
	void free();
	virtual void onNotify(GameEvent event);

private:
	Mix_Chunk* mBounceSound,* mGameOverSound,* mVictorySound;
	Mix_Music* mGameMusic;
};

#endif /* _MIXER_H */