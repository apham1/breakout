#ifndef _TEXTURE_H
#define _TEXTURE_H

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <string>

class Texture {
public:
	Texture();
	~Texture();

	void render(SDL_Renderer* renderer, int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
	bool loadMediaFromFile(SDL_Renderer* renderer, std::string file);
	bool loadFontFromFile(SDL_Renderer* renderer, TTF_Font* font, std::string text, SDL_Color color = { 0, 0, 0 });
	void setAlpha(Uint8 alpha);
	void free();
	
	int getWidth();
	int getHeight();

private:
	SDL_Texture* mTexture;
	int mWidth, mHeight;
};

#endif /* _TEXTURE_H */