#include "Player.h"

Player::Player() {
	mTexture = NULL;
	mPosX = 0;
	mPosY = 0;
	mVelX = 0;
	mWidth = 0;
	mHeight = 0;
	mCollider.w = 0;
	mCollider.h = 0;
}

void Player::setTexture(Texture* texture) {
	mTexture = texture;
	reset();
	mWidth = texture->getWidth();
	mHeight = texture->getHeight();
	mCollider.w = mWidth;
	mCollider.h = mHeight;
	mCollider.x = mPosX;
	mCollider.y = mPosY;
}

void Player::handleEvent(SDL_Event &e) {
	mVelX = 0;
	if (mCurrentKeyStates[SDL_SCANCODE_A]) {
		mVelX -= PLAYER_SPEED;
	}

	if (mCurrentKeyStates[SDL_SCANCODE_D]) {
		mVelX += PLAYER_SPEED;
	}

	if (mVelX < -PLAYER_SPEED) {
		mVelX = -PLAYER_SPEED;
	}

	if (mVelX > PLAYER_SPEED) {
		mVelX = PLAYER_SPEED;
	}
}

void Player::move() {
	mPosX += mVelX;
	mCollider.x = mPosX;

	if (mPosX < 0) {
		mPosX = 0;
		mCollider.x = mPosX;
	} else if (mPosX > SCREEN_WIDTH - mWidth) {
		mPosX = SCREEN_WIDTH - mWidth;
		mCollider.x = mPosX;
	}
}

void Player::reset() {
	mPosX = SCREEN_WIDTH / 2 - mTexture->getWidth() / 2;
	mPosY = SCREEN_HEIGHT - mTexture->getHeight() * 2.5f;
}

void Player::render(SDL_Renderer* renderer, double time) {
	int renderedX = (int)(mPosX + mVelX * time);
	
	//This is bad: collision checking in the render function
	if (renderedX < 0) {
		renderedX = 0;
	} else if (renderedX > SCREEN_WIDTH - mWidth) {
		renderedX = SCREEN_WIDTH - mWidth;
	}

	mTexture->render(renderer, renderedX, (int)mPosY);
}

int Player::getPosX() {
	return (int)mPosX;
}

int Player::getPosY() {
	return (int)mPosY;
}

int Player::getWidth() {
	return mWidth;
}

int Player::getHeight() {
	return mHeight;
}

float Player::getVelocity() {
	return mVelX;
}

SDL_Rect Player::getCollisionBox() {
	return mCollider;
}