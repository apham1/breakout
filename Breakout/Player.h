#ifndef _PLAYER_H
#define _PLAYER_H

#include "Texture.h"
#include "Constants.h"

class Player {
public:
	const float PLAYER_SPEED = 12.0;

	Player();

	void setTexture(Texture* texture);
	void handleEvent(SDL_Event &e);
	void move();
	void reset();
	void render(SDL_Renderer* renderer, double time);

	int getPosX();
	int getPosY();
	int getWidth();
	int getHeight();
	float getVelocity();
	SDL_Rect getCollisionBox();

private:
	float mPosX, mPosY;
	float mVelX;
	Texture* mTexture;
	int mWidth, mHeight;
	const Uint8* mCurrentKeyStates = SDL_GetKeyboardState(NULL);
	SDL_Rect mCollider;
};

#endif /* _PLAYER_H */