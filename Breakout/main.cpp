#include "main.h"

int main(int argc, char* args[]) {
	Breakout game;

	if (!game.init()) {
		printf("Failed to initialize game");
	} else {
		bool quit = false;
		double previous = SDL_GetTicks();
		double lag = 0.0;
		game.toggleMusic();
		game.levelUp();

		//While the application is running
		while (!quit) {
			double current = SDL_GetTicks();
			double elapsed = current - previous;
			previous = current;
			lag += elapsed;

			if (!game.handleEvent()) {
				quit = true;
			}

			while (lag >= MS_PER_UPDATE) {
				game.update();
				lag -= MS_PER_UPDATE;
			}

			game.render(lag / MS_PER_UPDATE);
		}
	}

	return 0;
}