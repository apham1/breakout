#include "Timer.h"

Timer::Timer() {
	mStartTicks = 0;
	mPausedTicks = 0;
	mPaused = false;
	mStarted = true;
}

void Timer::start() {
	mStarted = true;
	mPaused = false;

	mStartTicks = SDL_GetTicks();
	mPausedTicks = 0;
}

void Timer::stop() {
	mStarted = false;
	mPaused = false;

	mStartTicks = 0;
	mPausedTicks = 0;
}

void Timer::pause() {
	if (mStarted && !mPaused) {
		mPaused = true;

		//Calculate the paused ticks
		mPausedTicks = SDL_GetTicks() - mStartTicks;
		mStartTicks = 0;
	}
}

void Timer::unpause() {
	if (mStarted && mPaused) {
		mPaused = false;

		//Reset the starting ticks
		mStartTicks = SDL_GetTicks() - mPausedTicks;

		//Reset the paused ticks
		mPausedTicks = 0;
	}
}

Uint32 Timer::getTicks() {
	Uint32 time = 0;

	if (mStarted) {
		if (mPaused) {
			//Sets time to the number of ticks when the timer was paused
			time = mPausedTicks;
		} else {
			//Sets time to the current time minus the start time;
			time = SDL_GetTicks() - mStartTicks;
		}
	}

	return time;
}

bool Timer::isStarted() {
	return mStarted;
}

bool Timer::isPaused() {
	return mPaused && mStarted;
}