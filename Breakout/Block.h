#ifndef _BLOCK_H
#define _BLOCK_H

#include "Texture.h"
#include "Subject.h"

const int BASE_BLOCK_HEALTH = 1;

class Block : public Subject {
public:
	Block();
	Block(int x, int y, Texture* texture);
	~Block();

	void setTexture(Texture* texture);
	void render(SDL_Renderer* renderer);
	void spawn(int x, int y);
	void lowerHealth(int health);
	SDL_Rect getCollisionBox();
	bool isActive();
	void free();

	void notify(GameEvent event) {
		std::vector<Observer*>::iterator it;
		for (it = observers.begin(); it != observers.end(); ++it) {
			(*it)->onNotify(event);
		}
	}

private:
	int mPosX, mPosY;
	int mHealth;
	Texture* mTexture;
	SDL_Rect mCollider;
	bool mActive;
};

#endif _BLOCK_H