#include "Texture.h"

Texture::Texture() {
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

Texture::~Texture() {
	free();
}

void Texture::render(SDL_Renderer* renderer, int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip){
	SDL_Rect renderQuad = { x, y, mWidth, mHeight };
	SDL_RenderCopyEx(renderer, mTexture, clip, &renderQuad, angle, center, flip);
}

bool Texture::loadMediaFromFile(SDL_Renderer* renderer, std::string file) {
	bool success = true;

	free();

	SDL_Surface* image = NULL;
	image = IMG_Load(file.c_str());
	SDL_SetColorKey(image, SDL_TRUE, SDL_MapRGB(image->format, 0x00, 0xFF, 0x40));

	mWidth = image->w;
	mHeight = image->h;
	mTexture = SDL_CreateTextureFromSurface(renderer, image);
	SDL_FreeSurface(image);

	if (mTexture == NULL){
		success = false;
	}

	return success;
}

bool Texture::loadFontFromFile(SDL_Renderer* renderer, TTF_Font* font, std::string text, SDL_Color color) {
	bool success = true;

	free();

	SDL_Surface* textSurface = NULL;
	textSurface = TTF_RenderText_Blended(font, text.c_str(), color);

	mWidth = textSurface->w;
	mHeight = textSurface->h;

	mTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
	SDL_FreeSurface(textSurface);

	if (mTexture == NULL) {
		success = false;
	}

	return success;
}

void Texture::setAlpha(Uint8 alpha) {
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

void Texture::free() {
	if (mTexture != NULL) {
		SDL_DestroyTexture(mTexture);
		mWidth = 0;
		mHeight = 0;
	}
}

int Texture::getWidth() {
	return mWidth;
}

int Texture::getHeight() {
	return mHeight;
}