#ifndef _BALL_H
#define _BALL_H

#include <vector>
#include <SDL_mixer.h>
#include "Texture.h"
#include "Constants.h"
#include "Circle.h"
#include "GameMath.h"
#include "Block.h"
#include "Subject.h"
#include "Time.h"

const float BALL_SPEED = 8.0;

class Ball : public Subject{
public:
	Ball();
	~Ball();

	void setTexture(Texture* texture);
	void launch();
	void move(int x, int y, SDL_Rect &player, std::vector<Block> &blocks/*, std::vector<Block> &blueBlocks*/);
	void handleEvent(SDL_Event &e);
	void handleCollision(SDL_Rect &player, Block* block);
	void render(SDL_Renderer* renderer, double time, float playerVelX);
	bool isActive();
	void reset();
	//SDL_Rect getCollisionBox();
	void shiftColliders();
	Circle& getCollisionCircle();
	bool checkCollision(Circle a, SDL_Rect b, SDL_Point &p);


private:
	float mPosX, mPosY;
	float mVelX, mVelY;
	double mAngle;
	int mWidth, mHeight;
	/*Mix_Chunk* mBounceSound;*/
	bool mActive, mCollidedBlock, mCollidedPlayer, mCollidedTopBorder, mCollidedSideBorder, mReset;
	Texture* mTexture;
	//SDL_Rect mCollider;
	Circle mCollider;
};

#endif /* _BALL_H */