#ifndef _OBSERVER_H
#define _OBSERVER_H

enum GameEvent{
	BOUNCE,
	BLOCK_HIT,
	BALL_OUT_OF_BOUNDS,
	GAME_OVER,
	RESTART_GAME,
	WIN
};

class Observer {
public:
	virtual ~Observer(){}
	virtual void onNotify(GameEvent event) = 0;
};

#endif /* _OBSERVER_H */