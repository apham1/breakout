#ifndef _MAIN_H
#define _MAIN_H

#include <SDL.h>
#include "Breakout.h"

const double MS_PER_UPDATE = 1000 / 64;

#endif /* _MAIN_H */