#include "Mixer.h"

Mixer::Mixer() {
	mGameOverSound = NULL;
	mBounceSound = NULL;
	mGameMusic = NULL;
	mVictorySound = NULL;
}

bool Mixer::setSounds() {
	bool success = true;

	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
		SDL_Log("Unable to initialize mixer: %s\n", SDL_GetError());
		success = false;
	} else {
		mBounceSound = Mix_LoadWAV("bounce.wav");
		Mix_Volume(-1, 64);
		if (mBounceSound == NULL) {
			SDL_Log("Failed to load bounce sound effect! SDL_mixer Error: %s\n", Mix_GetError());
			success = false;
		} else {
			mGameOverSound = Mix_LoadWAV("youdied.wav");
			if (mGameOverSound == NULL) {
				SDL_Log("Failed to load game over sound effect: SDL_mixer Error: %s\n", Mix_GetError());
				success = false;
			} else {
				mVictorySound = Mix_LoadWAV("victory.wav");
				if (mVictorySound == NULL) {
					SDL_Log("Failed to load victory sound effect: SDL_mixer Error: %s\n", Mix_GetError());
					success = false;
				} else {
					mGameMusic = Mix_LoadMUS("gamemusic1.wav");
					Mix_VolumeMusic(75);
					if (mGameMusic == NULL) {
						SDL_Log("Failed to load music! SDL_mixer Error: %s\n", Mix_GetError());
						success = false;
					}
				}
			}
		}
	}

	return success;
}

void Mixer::setVolume(int volume) {
	Mix_VolumeMusic(volume);
}

void Mixer::toggleMusic() {
	if (Mix_PlayingMusic() == 0) {
		Mix_PlayMusic(mGameMusic, -1);
	} else {
		Mix_HaltMusic();
	}
}

void Mixer::pauseMusic() {
	Mix_PauseMusic();
}

void Mixer::unPauseMusic() {
	Mix_ResumeMusic();
}

void Mixer::free() {
	Mix_FreeChunk(mBounceSound);
	Mix_FreeChunk(mGameOverSound);
	Mix_FreeMusic(mGameMusic);
	Mix_Quit();
}

void Mixer::onNotify(GameEvent event) {
	switch (event) {
		case BOUNCE:
			Mix_PlayChannel(-1, mBounceSound, 0);
			break;
		case GAME_OVER:
			Mix_PlayChannel(-1, mGameOverSound, 0);
			toggleMusic();
			break;
		case RESTART_GAME:
			Mix_HaltChannel(-1);
			toggleMusic();
			break;
		case WIN:
			Mix_PlayChannel(-1, mVictorySound, 0);
			toggleMusic();
			break;
	}
}