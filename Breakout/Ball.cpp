#include "Ball.h"

Ball::Ball() {
	mTexture = NULL;
	mPosX = 0.0;
	mPosY = 0.0;
	mVelX = 0.0;
	mVelY = 0.0;
	mWidth = 0;
	mHeight = 0;
	mActive = false;
	mCollidedPlayer = false;
	mCollidedBlock = false;
	mCollidedTopBorder = false;
	mCollidedSideBorder = false;
	mCollider.r = 0;
	mReset = false;
	srand(time(NULL));

	//Jump to a psuedo-random section in the sequence
	for (int i = 0; i < rand() % 30; ++i) {
		rand();
	}
}

Ball::~Ball() {

}

void Ball::setTexture(Texture* texture) {
	mTexture = texture;
	mWidth = mTexture->getWidth();
	mHeight = mTexture->getHeight();
	mCollider.r = mWidth / 2;
	mPosX = SCREEN_WIDTH / 2;
	mPosY = SCREEN_HEIGHT / 2;
	shiftColliders();
}

void Ball::handleEvent(SDL_Event &e) {
	if (e.type == SDL_KEYDOWN) {
		switch (e.key.keysym.sym) {
			case SDLK_SPACE:
				launch();
				break;
		}
	}
}

void Ball::launch() {
	if (!mActive) {
		mActive = true;
		mReset = false;
		mVelX = ((float)rand() / (float)RAND_MAX - .5f) * BALL_SPEED;
		mVelY = -BALL_SPEED;
	}
}

void Ball::move(int x, int y, SDL_Rect &player, std::vector<Block> &blocks) {
	shiftColliders();
	handleCollision(player, &Block());

	std::vector<Block>::iterator it;
	for (it = blocks.begin(); it != blocks.end(); it++) {
		handleCollision(player, &(*it));
	}

	if (!mActive) {
		mPosX = x - mWidth / 2;
		mPosY = y - mHeight / 2;
	} else if (mActive) {
		if (mPosX <= -100 || mPosX + mWidth >= SCREEN_WIDTH + 100 || mPosY <= -100 || mPosY >= SCREEN_HEIGHT) {
			reset();
		} else {
			mPosX += mVelX;
			mPosY += mVelY;
			shiftColliders();
		}
	}
}

bool Ball::checkCollision(Circle a, SDL_Rect b, SDL_Point &p) {
	bool collision = false;
	int cX, cY;

	//Finds the closest x offset
	if (a.x < b.x) {
		cX = b.x;
	} else if (a.x > b.x + b.w) {
		cX = b.x + b.w;
	} else {
		cX = a.x;
	}

	//Finds the closest y offset
	if (a.y < b.y) {
		cY = b.y;
	} else if (a.y > b.y + b.h) {
		cY = b.y + b.h;
	} else {
		cY = a.y;
	}

	if (distanceSquared(a.x, a.y, cX, cY) < a.r * a.r) {
		collision = true;
	}

	p.x = cX;
	p.y = cY;

	return collision;
}

void Ball::handleCollision(SDL_Rect &player, Block* block) {
	//Collision with screen borders
	if (mPosX <= 0 || mPosX >= SCREEN_WIDTH - mWidth) {
		if (!mCollidedTopBorder) {
			mVelX = -mVelX;
			mCollider.x = mPosX + mWidth / 2;
			notify(BOUNCE);
		}
		mCollidedTopBorder = true;
	} else {
		mCollidedTopBorder = false;
	}

	if (mPosY <= 0) {
		if (!mCollidedSideBorder) {
			mVelY = -mVelY;
			mCollider.y = mPosY + mHeight / 2;
			notify(BOUNCE);
		}
		mCollidedSideBorder = true;
	} else if (mPosY + mHeight / 2 >= SCREEN_HEIGHT) {
		if (!mReset) {
			notify(BALL_OUT_OF_BOUNDS);
			reset();
			mReset = true;
		}
	} else {
		mCollidedSideBorder = false;
	}

	SDL_Point collisionInfo;

	shiftColliders();
	//Collision with player
	if (checkCollision(mCollider, player, collisionInfo)) {
		float halfWidth = player.w / 2;
		float playerX = player.x + halfWidth;

		float velocityFactor = (((float)mCollider.x - playerX) / halfWidth) * 1.1;

		if (!mCollidedPlayer) {
			mVelX = BALL_SPEED * velocityFactor;
			mVelY = -mVelY;
			notify(BOUNCE);
		}

		mCollidedPlayer = true;
	} else {
		mCollidedPlayer = false;
	}

	//Collision with block
	SDL_Rect blockCollisionBox = block->getCollisionBox();
	if (checkCollision(mCollider, blockCollisionBox, collisionInfo)) {
		bool topCollision = collisionInfo.y == blockCollisionBox.y;
		bool bottomCollision = collisionInfo.y == blockCollisionBox.y + blockCollisionBox.h;
		bool leftCollision = collisionInfo.x == blockCollisionBox.x;
		bool rightCollision = collisionInfo.x == blockCollisionBox.x + blockCollisionBox.w;

		if ((topCollision || bottomCollision) && (leftCollision || rightCollision)) {
			if (!mCollidedBlock) {
				if (topCollision) {
					if (leftCollision) {
						if (mVelX >= 0) {
							mVelX = -mVelX;
						}
						
						if (mVelY >= 0) {
							mVelY = -mVelY;
						}
					}

					if (rightCollision) {
						if (mVelX <= 0) {
							mVelX = -mVelX;
						}

						if (mVelY >= 0) {
							mVelY = -mVelY;
						}
					}
				}

				if (bottomCollision) {
					if (leftCollision) {
						if (mVelX >= 0) {
							mVelX = -mVelX;
						}

						if (mVelY <= 0) {
							mVelY = -mVelY;
						}
					}

					if (rightCollision) {
						if (mVelX <= 0) {
							mVelX = -mVelX;
						}

						if (mVelY <= 0) {
							mVelY = -mVelY;
						}
					}
				}

				mCollidedBlock = true;
				notify(BOUNCE);
				block->lowerHealth(1);
				printf("corner\n");
			}
		} else if (topCollision || bottomCollision) {
			if (!mCollidedBlock) {
				mVelY = -mVelY;
				mCollidedBlock = true;
				notify(BOUNCE);
				block->lowerHealth(1);
				printf("top/bottom\n");
			}
		} else if (leftCollision || rightCollision){
			if (!mCollidedBlock) {
				mVelX = -mVelX;
				mCollidedBlock = true;
				notify(BOUNCE);
				block->lowerHealth(1);
				printf("side\n");
			}
		}
	} else {
		mCollidedBlock = false;
	}
}

void Ball::render(SDL_Renderer* renderer, double time, float playerVelX) {
	int renderedX = mPosX;
	int renderedY = mPosY;

	if (!mActive) {
		renderedX = mPosX + playerVelX * time;

		if (renderedX - 50 + mWidth / 2 < 0) {
			renderedX = 50 - mWidth / 2;
		} else if (renderedX > SCREEN_WIDTH - 50 - mWidth / 2) {
			renderedX = SCREEN_WIDTH - 50 - mWidth / 2;
		}
	} else {
		renderedX = mPosX + mVelX * time;
		renderedY = mPosY + mVelY * time;
	}

	mTexture->render(renderer, renderedX, renderedY);
}

bool Ball::isActive() {
	return mActive;
}

void Ball::reset() {
	mActive = false;
	mVelX = 0;
	mVelY = 0;
}

Circle& Ball::getCollisionCircle() {
	return mCollider;
}

void Ball::shiftColliders() {
	mCollider.x = mPosX + mWidth / 2;
	mCollider.y = mPosY + mHeight / 2;
}