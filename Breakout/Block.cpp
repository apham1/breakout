#include "Block.h"

Block::Block() {
	mPosX = 0;
	mPosY = 0;
	mTexture = NULL;
	mCollider.x = mPosX;
	mCollider.y = mPosY;
	mCollider.w = 0;
	mCollider.h = 0;
	mActive = false;
	mHealth = BASE_BLOCK_HEALTH;
}

Block::Block(int x, int y, Texture* texture) {
	mPosX = x;
	mPosY = y;
	mTexture = texture;
	mCollider.x = mPosX;
	mCollider.y = mPosY;
	mCollider.w = texture->getWidth();
	mCollider.h = texture->getHeight();
	mActive = true;
	mHealth = BASE_BLOCK_HEALTH;
}

Block::~Block() {
	free();
}

void Block::setTexture(Texture* texture) {
	mTexture = texture;
	mCollider.w = mTexture->getWidth();
	mCollider.h = mTexture->getHeight();
}

void Block::spawn(int x, int y) {
	mHealth = BASE_BLOCK_HEALTH;
	mActive = true;
	mPosX = x;
	mPosY = y;
}

void Block::render(SDL_Renderer* renderer) {
	mTexture->render(renderer, mPosX, mPosY);
}

SDL_Rect Block::getCollisionBox() {
	return mCollider;
}

bool Block::isActive() {
	if (mHealth <= 0) {
		mActive = false;
		free();
	}
	return mActive;
}


void Block::lowerHealth(int health) {
	mHealth -= health;
	notify(BLOCK_HIT);
}

void Block::free() {
	mPosX = 0;
	mPosY = 0;
	mTexture = NULL;
	mCollider.x = 0;
	mCollider.y = 0;
	mCollider.w = 0;
	mCollider.h = 0;
	mHealth = 0;
}