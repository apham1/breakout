#ifndef _SUBJECT_H
#define _SUBJECT_H

#include <vector>
#include "Observer.h"

const int MAX_OBSERVERS = 10;

class Subject {
public:
	Subject(){
		observers.reserve(MAX_OBSERVERS);
	}

	void addObserver(Observer* observer) {
		observers.push_back(observer);
	}

	void removeObserver(Observer* observer) {
		auto it = std::find(observers.begin(), observers.end(), observer);
		if (it != observers.end()) {
			observers.erase(it);
		}
	}

protected:
	void notify(GameEvent event) {
		std::vector<Observer*>::iterator it;
		for (it = observers.begin(); it != observers.end(); ++it) {
			(*it)->onNotify(event);
		}
	}

//private:
	std::vector<Observer*> observers;
	int numObservers;
};

#endif /* _SUBJECT_H */