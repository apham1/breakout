#ifndef _BREAKOUT_H
#define _BREAKOUT_H

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <Time.h>
#include <stdio.h>
#include <string>
#include <vector>
#include "Texture.h"
#include "Player.h"
#include "Ball.h"
#include "Block.h"
#include "Mixer.h"
#include "Timer.h"

class Breakout : public Observer, public Subject{
public:
	Breakout();
	~Breakout();

	bool init();
	bool loadMedia();
	void spawnEntities();
	bool handleEvent();
	void update();
	void render(double time);
	void toggleMusic();
	void addObservers();
	void gameOver();
	void restartGame();
	void setText();
	void levelUp();
	void win();
	bool loadLevelTextures();
	virtual void onNotify(GameEvent event);

private:
	SDL_Window* mWindow;
	SDL_Renderer* mRenderer;
	TTF_Font* mFont;
	Mixer mMixer;
	Timer mTimer;
	Texture mPlayerTexture, mBallTexture, mRedBlockTexture, mOrangeBlockTexture, mYellowBlockTexture,
		mGreenBlockTexture, mSkyBlueBlockTexture, mBlueBlockTexture, mPurpleBlockTexture, mPointsTexture,
		mBallsTexture, mGameOverTexture, mPlayAgainTexture, mPlayAgainPushedTexture, mLevelTexture,
		mOneTexture, mTwoTexture, mThreeTexture, mFourTexture, mFiveTexture, mSixTexture, mSevenTexture,
		mEightTexture, mNineTexture, mZeroTexture, mLevelTextTexture, mNumberSheetTexture, mWinTexture;
	Player mPlayer;
	Ball mBall;
	int mPoints, mBalls, mMouseX, mMouseY, mBlocksLeft, mLevel, mRows;
	float mGameOverAlpha;
	std::vector<Block> mBlocks;
	Block* block;
	bool mGameOver, mLevelUp, mWin;
};

#endif /* _BREAKOUT_H */